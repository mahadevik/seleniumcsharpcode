﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_001.PageObjects
{
    internal class Profilepage:Homepage
    {
        IWebDriver driver;
        IJavaScriptExecutor js=null;
        WebDriverWait wait=null;
        string profile_btn = " //*[text()=' My Profile'][@class=' waves-effect']";
        string first_name = "//input[@name='firstname']";
        string last_name = "//input[@name='lastname']";
        string phone = "//input[@name='phone']";
        string country = "//span[@id='select2-from_country-container']";
        string state = "//input[@name='state']";
        string city = "//input[@name='city']";
        string fax = "//input[@name='fax']";
        string postal_code = "//input[@name='zip']";
        string address1 = "//input[@name='address1']";
        string address2 = "//input[@name='address2']";
        string update_profile_btn = "//button[text()='Update Profile']";
        string logout_btn = " //*[text()=' Logout'][@class=' waves-effect']";
        string profile_success = "//*[@class='alert alert-success']";
       
        public Profilepage(IWebDriver driver) : base(driver)
        {
            this.driver = driver;

        }
        public void ClickProfilebtn()
        {
            driver.FindElement(By.XPath(profile_btn)).Click();
        }
        public void EnterFirstName()
        {
            IWebElement user = driver.FindElement(By.XPath(first_name));
            user.Clear();
            user.SendKeys("Guna");
        }
        public void EnterLastName()
        {
            IWebElement last = driver.FindElement(By.XPath(last_name));
            last.Clear();
            last.SendKeys("sekaran");

        }
        public void EnterPhone()
        {
            IWebElement phonetxt = driver.FindElement(By.XPath(phone));
            phonetxt.Clear();
            phonetxt.SendKeys("1234567890");
        }
        public void Entercountry()
        {
            IWebElement coun = driver.FindElement(By.XPath(country));
            js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("arguments[0].scrollIntoView(false)", coun);
            js.ExecuteScript("arguments[0].value='india';", coun);

        }
        public void Enterstate()
        {
            js = (IJavaScriptExecutor)driver;
            IWebElement sta = driver.FindElement(By.XPath(state));
            js.ExecuteScript("arguments[0].value='tamilnadu';", sta);
        }
        public void EnterCity()
        {
            driver.FindElement(By.XPath(city)).SendKeys("cbe");
        }
        public void EnterFax()
        {
            driver.FindElement(By.XPath(fax)).SendKeys("12345");
        }
        public void EnterPostalcode()
        {
            IWebElement postal = driver.FindElement(By.XPath(postal_code));
            postal.Clear();
            postal.SendKeys("12345");
        }
        public void EnterAddress1()
        {
            IWebElement add1 = driver.FindElement(By.XPath(address1));
            add1.Clear();
            add1.SendKeys("coimbatore");
        }
        public void EnterAddress2()
        {
            IWebElement add2 = driver.FindElement(By.XPath(address2));
            add2.SendKeys("PNPalayam");
        }
        public void UpdateProfile()
        {
           IWebElement update= driver.FindElement(By.XPath(update_profile_btn));
            js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("arguments[0].click()", update);
        }
        public void Logout()
        {
            driver.FindElement(By.XPath(logout_btn)).Click();
        }
        public void Profile_Successs_Massege()
        {
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            IWebElement success = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath(profile_success)));
            string getText=  success.Text;
            Console.WriteLine(getText);
        }
        






    }
}
