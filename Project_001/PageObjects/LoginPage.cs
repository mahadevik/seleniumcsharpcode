﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_001.PageObjects
{
    internal class Loginpage : Profilepage
    {
        IWebDriver driver;
        IJavaScriptExecutor? js =null;
        WebDriverWait? wait =null;

        string email_box = "//input[@name='email'][@placeholder='Email']";
        string password_box = "//input[@name='password'][@placeholder='Password']";
        string login_btn = "//span[text()='Login']";
        string dash_board = " //*[text()=' Dashboard'][@class=' waves-effect']";
        string time = "//*[@id='ct']";

        public Loginpage(IWebDriver driver) : base(driver)
        {
            this.driver = driver;
        }
        public void EnterUsername()
        {
            driver.FindElement(By.XPath(email_box)).SendKeys("admin@phptravels.com");
        }
        public void Enterpassword()
        {
            driver.FindElement(By.XPath(password_box)).SendKeys("demoadmin");
        }
        public void ClickLoginbtn()
        {
            IWebElement btn = driver.FindElement(By.XPath(login_btn));
            js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("arguments[0].click()", btn);
        }
        public void DashBoard()
        {
            Boolean dashboard_check = driver.FindElement(By.XPath(dash_board)).Enabled;
            Console.WriteLine(dashboard_check);
            string title = driver.Title;
            Console.WriteLine(title);
            Assert.AreEqual(title, "Dashboard - PHPTRAVELS");
        }
        public void GetTimestamp()
        {
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            IWebElement timestamp = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath(time)));
            string massege = timestamp.Text;
            Console.WriteLine(massege);
        }





















    }
}
