﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_001.PageObjects
{
    internal class Homepage
    {
        IWebDriver driver;
        IJavaScriptExecutor? js =null;
        SelectElement? select =null;
        WebDriverWait? wait=null;
        string home_Btn = "//a[text()='Home']";
        string hotals_link = "//*[text()=' Hotels']";
        string search_by_city = "//*[text()=' Search by City'][@id='select2-hotels_city-container']";
        string send_cityname = "//*[@class='select2-search__field']";
        string cityname = "//*[@id='select2-hotels_city-results']";
        string checkin_btn = "//input[@id='checkin']";
        string checkout_btn = "//input[@id='checkout']";
        string travelers = "//a[@class='dropdown-toggle dropdown-btn travellers waves-effect']";
        string rooms = "rooms";
        string adults = "adults";
        string nationality = "//select[@id='nationality']";
        string submit = "//*[@id='submit']";

        public Homepage(IWebDriver driver)
        {
            this.driver = driver;
        }
        public void ClickHomeButton()
        {
            driver.FindElement(By.XPath(home_Btn)).Click();
        }
        public void ClickHotel()
        {
            driver.FindElement(By.XPath(hotals_link)).Click();  
        }
        public void SearchCity()
        {
            IWebElement city = driver.FindElement(By.XPath(search_by_city));
            city.Click();
            IWebElement send= driver.FindElement(By.XPath(send_cityname));
            js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("arguments[0].value='mum';", send);
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(60));
            IWebElement SearchResult = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath(cityname)));
           SearchResult.Click();
        }
        public void ClickCheckin()
        {
           IWebElement element= driver.FindElement(By.XPath(checkin_btn));
            js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("arguments[0].value='30-06-2022';", element);

        }
        public void ClickCheckout()
        {
           IWebElement element1= driver.FindElement(By.XPath(checkout_btn));
            js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("arguments[0].value='01-07-2022';", element1);
        }
        public void ClickTravellers()
        {
            js = (IJavaScriptExecutor)driver;
            driver.FindElement(By.XPath(travelers)).Click();
            IWebElement element= driver.FindElement(By.Id(rooms));
            js.ExecuteScript("arguments[0].value='3';", element);
            IWebElement element1 = driver.FindElement(By.Id(adults));
            js.ExecuteScript("arguments[0].value='3';", element1);
            IWebElement nation = driver.FindElement(By.XPath(nationality));
            js.ExecuteScript("arguments[0].scrollIntoView(false)", nation);
            select = new SelectElement(nation);
            select.SelectByText("India");
        }
        public void ClickSearchButton()
        {
            IWebElement btn = driver.FindElement(By.XPath(submit));
            js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("arguments[0].click()", btn);
        }



    }
}
