﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Project_001.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_001
{
    internal class LoginTest
    {
        IWebDriver _driver=default!;
        Loginpage log=default!;
        [SetUp]
        public void OpenBrowser()
        {
             _driver = new ChromeDriver();
            _driver.Navigate().GoToUrl("https://www.phptravels.net/login");
            _driver.Manage().Window.Maximize();
            /*_driver.Manage().Timeouts().ImplicitWait=TimeSpan.FromSeconds(2);*/
        }
        [Test]
        public void GotoLoginpage()
        {
            log = new Loginpage(GoDriver());
            log.EnterUsername();
            log.Enterpassword();
            log.ClickLoginbtn();
            log.DashBoard();
            log.GetTimestamp();
        }
        [Test]
        public void GotoProfile() {
            log = new Loginpage(GoDriver());
            log.EnterUsername();
            log.Enterpassword();
            log.ClickLoginbtn();
            log.DashBoard();
            log.ClickProfilebtn();
            log.EnterFirstName();
            log.EnterLastName();
            log.EnterPhone();
            log.Entercountry();
            log.EnterCity();
            log.Enterstate();
            log.EnterFax();
            log.EnterPostalcode();
            log.EnterAddress1();
            log.EnterAddress2();
            log.UpdateProfile();
            log.Profile_Successs_Massege();
            log.GetTimestamp();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(2);
            log.Logout();

        }
        [Test]
        public void GotoHomePage()
        {
            log = new Loginpage(GoDriver());
            log.ClickHomeButton();
            log.ClickHotel();
            log.SearchCity();
            log.ClickCheckin();
            log.ClickCheckout();
            log.ClickTravellers();
            log.ClickSearchButton();

        }
        [TearDown]
        public void CloseBrowser()
        {
            _driver.Close();
        }
        public IWebDriver GoDriver() { return _driver; }

    }
}
